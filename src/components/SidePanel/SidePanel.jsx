const SidePanel = () => {
  const onDragStart = (event, nodeType) => {
    console.log("drag start", event);
    event.dataTransfer.setData("application/reactflow", nodeType);
    event.dataTransfer.effectAllowed = "move";
  };

  return (
    <aside className="w-96 bg-gray-200">
      <div className="text-xl font-bold p-4">Add new elements</div>
      <div
        className="bg-blue-500 text-white p-2 rounded-md m-2 cursor-pointer"
        onDragStart={(event) => onDragStart(event, "input")}
        draggable
      >
        Input Node
      </div>
      <div
        className="bg-green-500 text-white p-2 rounded-md m-2 cursor-pointer"
        onDragStart={(event) => onDragStart(event, "default")}
        draggable
      >
        Default Node
      </div>
      <div
        className="bg-red-500 text-white p-2 rounded-md m-2 cursor-pointer"
        onDragStart={(event) => onDragStart(event, "output")}
        draggable
      >
        Output Node
      </div>
    </aside>
  );
};

export default SidePanel;
