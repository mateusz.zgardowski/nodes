const GrindingNode = () => {
  return (
    <div className="custom-node">
      <div className="custom-node__header">
        <h4>Grinding</h4>
      </div>
      <div className="custom-node__content">
        <p>Grinding content</p>
      </div>
    </div>
  );
};

export default GrindingNode;
