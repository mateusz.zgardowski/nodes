import React, { memo } from "react";
import { Handle, Position } from "reactflow";
import WateringNode from "./NodeTypes/WateringNode";
import TableNode from "./NodeTypes/TableNode";
import GrindingNode from "./NodeTypes/GrindingNode";

const CustomNode = ({ type, data }) => {
  console.log(data.nodeType);

  const node = (data) => {
    switch (data.nodeType) {
      case "WateringNode":
        return <WateringNode data={data} />;
      case "TableNode":
        return <TableNode data={data} />;
      case "GrindingNode":
        return <GrindingNode data={data} />;
      default:
        return <div>Unknown node type</div>;
    }
  };

  return (
    <div className="px-4 py-2 shadow-md rounded-md bg-white border-2 border-stone-400">
      {/* <div className="flex">
        <div className="rounded-full w-12 h-12 flex justify-center items-center bg-gray-100">
          {data.emoji}
        </div>
        <div className="ml-2">
          <div className="text-lg font-bold">{data.name}</div>
          <div className="text-gray-500">{data.job}</div>
        </div>
      </div> */}
      {node(data)}

      <Handle
        type="target"
        position={Position.Left}
        className="w-2 !bg-teal-500 rounded-none h-10"
      />
      <Handle
        type="source"
        position={Position.Bottom}
        className="w-16 !bg-teal-500 rounded-none h-2"
      />
    </div>
  );
};

export default memo(CustomNode);
