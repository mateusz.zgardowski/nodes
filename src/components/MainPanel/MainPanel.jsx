import { Background, Controls } from "reactflow";

const MainPanel = () => {
  return (
    <div className="h-96 w-full">
      <Background />
      <Controls />
    </div>
  );
};

export default MainPanel;
